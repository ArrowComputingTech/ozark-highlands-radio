# This program downloads Ozark Highlands Radio. Works on Mac or Linux.
# It:
# 1.)Connects to the designated gmail.com account
# 2.)Gets links from newest email with OHR tag (set by filter in the gmail account.)
# 3.)Makes backup of old show.
# 4.)Downloads the show from Google Drive specified in the email to external drive.

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.auth.transport.requests import AuthorizedSession
from apiclient import errors
from apiclient.http import MediaIoBaseDownload
from bs4 import BeautifulSoup as bs4
import pickle, os.path, json, base64, requests, io, glob, shutil, re

os.chdir('/wherever/you/want/ohr')

def configure_backups():
    # Creates back up of last week show. Deletes previous backup.
    old_backup = glob.glob(os.path.join('./backups/'+'*.mp2'))
    print (old_backup)
    for file in old_backup:
        if os.path.exists(file):
            print ('Removing ',file,' from backup folder...')
            os.remove(file)
    current_backup = glob.glob(os.path.join('*.mp2'))
    for file in current_backup:
        if os.path.exists(file):
            print ('Putting ',file,' in backup folder.')
            destination = ('./backups/'+str(file))
            shutil.move(file, destination)

class GmailHandler():
    def get_gmail_creds(self):
        self.gmail_creds = None
        gc_token = './gmailcreds/token.pickle'
        SCOPE = ['https://www.googleapis.com/auth/gmail.readonly']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(gc_token):
            with open(gc_token, 'rb') as token:
                self.gmail_creds = pickle.load(token)
        if not self.gmail_creds or not self.gmail_creds.valid:
            # If creds expired and token exists, refresh creds.
            if self.gmail_creds and self.gmail_creds.expired and self.gmail_creds.refresh_token:
                self.gmail_creds.refresh(Request())
            else:
                # If there are no (valid) credentials available, let the user log in.
                # The following code will require user interaction, but may be necessary if on a new machine:
                print ('Credentials expired or invalid. Reauthorize credentials.')
                exit()
                #flow = InstalledAppFlow.from_client_secrets_file('./servicecreds/credentials.json', SCOPE)
                #self.gmail_creds = flow.run_local_server(port=0)
        # Save the credentials for the next run (This may need to be put back in place...)
        with open(gc_token, 'wb') as token:
            pickle.dump(self.gmail_creds, token, protocol=2)

    def get_gmail_message(self, gmail_creds):
        # Build Gmail service
        GMAIL_SERVICE = build('gmail', 'v1', credentials=gmail_creds)
        # Call the Gmail API and get newest OHR labeled message, get message ID, get message.
        gmail_result = GMAIL_SERVICE.users().messages().list(userId='me', maxResults=1, q='label:OHR').execute()
        message_id = gmail_result['messages'][0]['id']
        message = GMAIL_SERVICE.users().messages().get(userId='me', id=message_id).execute()
        # Get body of message, decode body text.
        payload = message['payload']['parts'][1]['body']['data']
        self.gmail_message = base64.urlsafe_b64decode(payload.encode('ASCII'))

class DriveFolderHandler:
    def get_folder_info(self, gmail_message):
        # Get links to MP3 files from email.
        soup = bs4(gmail_message, "html.parser")
        links=[]
        for link in soup.findAll('a', attrs={'href': re.compile("^https://drive.google.com")}):
            print(link.get('href'))
            links.append(link.get('href'))
        id_string = links[1].split("=", 1)
        self.linkID = id_string[1]
        self.folder_URL = ('https://www.googleapis.com/drive/v3/files/'+self.linkID)

class DriveClient():
    def connect(self, folder_info):
        # Get creds for Drive service and create it.
        self.folder_info = folder_info
        SCOPE = ['https://www.googleapis.com/auth/drive.readonly']
        dc_token = ('./drivecreds/token.pickle')
        self.drive_creds = None
        if os.path.exists(dc_token):
            with open(dc_token, 'rb') as token:
                self.drive_creds = pickle.load(token)
        if not self.drive_creds or not self.drive_creds.valid:
            if self.drive_creds and self.drive_creds.expired and self.drive_creds.refresh_token:
                self.drive_creds.refresh(Request())
            else:
                # The following code will require user interaction via GUI. Comment current print and exit lines and uncomment flow and self to run.
                print ("Credentials not found or expired. Reauthorize credentials.")
		        exit()
                #flow = InstalledAppFlow.from_client_secrets_file('./servicecreds/credentials.json', SCOPE)
                #self.drive_creds = flow.run_local_server(port=0)
            with open(dc_token, 'wb') as token:
                pickle.dump(self.drive_creds, token, protocol=2)
        self.service = build('drive', 'v3', credentials=self.drive_creds)
        self.audio_files = self.list_folder()

    def list_folder(self):
        # Get and return file ids from root folder.
        audio_files = []
        results = self.service.files().list(q="'"+self.folder_info.linkID+"'"+" in parents").execute()
        for file in results.get('files'):
            if file['mimeType']=='audio/mpeg':
                audio_files.append(file['id'])
        return audio_files

def get_gdrive_files(dc, file_IDs):
    # Set up destination file and call the download.
    i=1
    for file in file_IDs:
        request = dc.service.files().get_media(fileId=file)
        fh = io.BytesIO()
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        filepath=('./OHRpart'+str(i)+'.mp2')
        while done is False:
            print ("Downloading ", file)
            status, done = downloader.next_chunk()
        with io.open(filepath,'wb') as write_file:
            print ("Writing "+filepath+" to OHR folder.")
            fh.seek(0)
            write_file.write(fh.read())
            print ("Done.")
            i+=1

configure_backups()
gh = GmailHandler()
gh.get_gmail_creds()
gh.get_gmail_message(gh.gmail_creds)
fi = DriveFolderHandler()
fi.get_folder_info(gh.gmail_message)
dc = DriveClient()
dc.connect(fi)
get_gdrive_files(dc, dc.audio_files)
